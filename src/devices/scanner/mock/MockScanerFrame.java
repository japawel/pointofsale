package devices.scanner.mock;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;

public class MockScanerFrame extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JTextField codeField;
	
	public MockScanerFrame()
	{
		init();
	}
	
	private void init()
	{
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
		this.setSize(screenDimension.width/4, screenDimension.height/4);
		codeField = new JTextField();
		this.add(codeField);
		this.setLocation(screenDimension.width*1/2, 0);
		this.setTitle("Scanner");
		this.setVisible(true);
	}
	
	public InputStream getInputStream()
	{
		return new ScannerInputStream(codeField);
	}
	
	private class ScannerInputStream extends InputStream
	{
		final LinkedBlockingQueue<Character> sb = new LinkedBlockingQueue<Character>();
		private JTextField text;
		
		public ScannerInputStream(final JTextField text)
		{
			this.text = text;
			text.addKeyListener(new KeyAdapter() 
			{
				@Override
				public void keyReleased(KeyEvent e) 
				{
					sb.offer(e.getKeyChar());
					if (e.getKeyCode() == 10)
						sb.offer(e.getKeyChar());
				}
			});
		}
		
		@Override
		public int read() throws IOException 
		{
			int c = -1;
			try 
			{
				c = sb.take();            
		    } catch(InterruptedException ie) 
			{
		    	ie.printStackTrace();
		    }
			if (c == 10)
			{
				text.setText("");
				c = -1;
			}
		    return c;           
		}
	}

}
