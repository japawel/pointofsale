package devices.scanner.mock;

import java.io.InputStream;

import devices.scanner.Scanner;

public class MockScaner extends Scanner{

	private MockScanerFrame mockScanerFrame;
	
	public MockScaner()
	{
		mockScanerFrame = new MockScanerFrame();
	}
	
	@Override
	protected InputStream getInputStream() 
	{
		return mockScanerFrame.getInputStream();
	}

	
}
