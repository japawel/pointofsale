package devices.scanner;

import java.io.IOException;
import java.io.InputStream;

public abstract class Scanner {

	public String getCode() throws IOException
	{
		String code = "";
		java.util.Scanner sc = new java.util.Scanner(getInputStream());
        code += sc.nextLine();

        sc.close();
		return code;
	}
	
	protected abstract InputStream getInputStream(); 
}
