package devices.printer.mock;

import java.io.OutputStream;

import devices.printer.Printer;

public class MockPrinter extends Printer{

	private MockPrinterFrame mockPrinterFrame;
	
	public MockPrinter() 
	{
		mockPrinterFrame = new MockPrinterFrame();
	}
	
	@Override
	protected OutputStream getOutputStream() 
	{
		return mockPrinterFrame.getOutputStream();
	}

}