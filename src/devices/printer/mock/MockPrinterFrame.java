package devices.printer.mock;

import java.awt.Dimension;
import java.awt.Toolkit;

import devices.MockOutputDeviceFrame;

public class MockPrinterFrame extends MockOutputDeviceFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MockPrinterFrame()
	{
		super();
		Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(screenDimension.width*3/4, screenDimension.height/4);
		this.setTitle("Printer");
	}
}
