package devices.printer;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;

import database.Product;

public abstract class Printer {

	private static final String SUM_MESSAGE = "Suma: ";
	
	public void printRecipt(List<Product> products) throws IOException
	{
		String toWrite;
		BigDecimal sum = new BigDecimal(0);
		
		for(Product product: products)
		{
			toWrite = product.getName() + "\t" + product.getPriceStringFormat() + "\n";
			write(toWrite);
			sum = sum.add(product.getPrice());
		}
		write("\n");
		write(SUM_MESSAGE + "\t" + new DecimalFormat("0.00").format(sum));
	}
	
	private void write(String message) throws IOException
	{
		getOutputStream().write(message.getBytes());
	}
	
	protected abstract OutputStream getOutputStream();
	
}
