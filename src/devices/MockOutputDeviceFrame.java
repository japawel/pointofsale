package devices;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;
import java.io.OutputStream;

import javax.swing.JFrame;
import javax.swing.JTextArea;

public class MockOutputDeviceFrame extends JFrame{

	private static final long serialVersionUID = 1L;
	private JTextArea messageArea;
	private OutputStream outputStream;
	
	public MockOutputDeviceFrame()
	{
		init();
	}
	
	private void init()
	{
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
		this.setSize(screenDimension.width/4, screenDimension.height/4);
		messageArea = new JTextArea();
		messageArea.setEditable(false);
		this.add(messageArea);
		outputStream = new MessageOutputStream(messageArea);
		this.setLocation(screenDimension.width*3/4, 0);
		this.setVisible(true);
	}
	
	public void setMessage(String message)
	{
		messageArea.setText(message);
	}
	
	public OutputStream getOutputStream()
	{
		return outputStream;
	}
	
	private class MessageOutputStream extends OutputStream
	{
		private JTextArea textArea;
		
		public MessageOutputStream(JTextArea textArea)
		{
			this.textArea = textArea;
		}
		
		@Override
		public void write(int b) throws IOException 
		{
	        textArea.append(String.valueOf((char)b));
	        textArea.setCaretPosition(textArea.getDocument().getLength());
		}
		
	}
	
}
