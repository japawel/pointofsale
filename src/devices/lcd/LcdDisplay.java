package devices.lcd;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;

import database.Product;

public abstract class LcdDisplay {
	
	private final static String SUM_MESSAGE = "Suma: ";
	
	public void displayProduct(Product product) throws IOException
	{
		displayMessage(product.getName() + "\t" + product.getPriceStringFormat() + "\n");
	}
	
	public void displaySum(BigDecimal sum) throws IOException
	{
		displayMessage(SUM_MESSAGE + "\t" + new DecimalFormat("0.00").format(sum) + "\n");
	}
	
	public void displayError(ErrorCode errorCode) throws IOException
	{
		displayMessage(errorCode.toString() + "\n");
	}
	
	private void displayMessage(String message) throws IOException
	{
		getOutputStream().write(message.getBytes());
	}
	
	protected abstract OutputStream getOutputStream();
}
