package devices.lcd;

public enum ErrorCode {

	PRODUCT_NOT_FOUND("Product not found"),
	INVALID_BAR_CODE("Invalid bar-code");
	
	private String description;
	private ErrorCode(String description)
	{
		this.description = description;
	}
	
	@Override
	public String toString()
	{
		return description;
	}
}
