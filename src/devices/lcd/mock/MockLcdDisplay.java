package devices.lcd.mock;

import java.io.OutputStream;

import devices.lcd.LcdDisplay;

public class MockLcdDisplay extends LcdDisplay{

	private MockLcd mockLcd;
	
	public MockLcdDisplay() 
	{
		mockLcd = new MockLcd();
	}

	@Override
	protected OutputStream getOutputStream() 
	{
		return mockLcd.getOutputStream();
	}

}
