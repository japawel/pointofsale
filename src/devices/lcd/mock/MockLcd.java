package devices.lcd.mock;

import devices.MockOutputDeviceFrame;

public class MockLcd extends MockOutputDeviceFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MockLcd()
	{
		this.setTitle("LCD Display");
	}
}
