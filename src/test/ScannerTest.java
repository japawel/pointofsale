package test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;

import com.sun.xml.internal.messaging.saaj.util.ByteInputStream;

import devices.scanner.Scanner;

public class ScannerTest {

	@Test
	public void test() {
		
		final ByteInputStream is = new ByteInputStream();
		Scanner scanner = new Scanner() 
		{	
			@Override
			protected InputStream getInputStream() 
			{
				return is;
			}
		};
		String expectedInput = "Some expected code";
		is.setBuf(expectedInput.getBytes());
		String gotCode = null;
		try {
			gotCode = scanner.getCode();
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertEquals("Test scanner get code", expectedInput, gotCode);
	}

}
