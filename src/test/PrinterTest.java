package test;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import database.Product;
import devices.printer.Printer;

public class PrinterTest {

	@Test
	public void test() 
	{
		List<Product> products = new ArrayList<Product>();
		products.add(new Product("Name", "Code", new BigDecimal(120)));
		products.add(new Product("Name2", "Code2", new BigDecimal(140)));
		products.add(new Product("Name3", "Code3", new BigDecimal(1.50)));
		final ByteArrayOutputStream os = new ByteArrayOutputStream();
		Printer printer = new Printer() {
			@Override
			protected OutputStream getOutputStream() 
			{
				return os;
			}
		};
		try {
			printer.printRecipt(products);
		} catch (IOException e) {
			e.printStackTrace();
		}
		byte[] bytes = ((ByteArrayOutputStream) os).toByteArray();
		String displayText = new String(bytes);
		String expected = "Name\t120,00\nName2\t140,00\nName3\t1,50\n\nSuma: \t261,50";
		assertEquals("Test print", expected, displayText);
	}

}
