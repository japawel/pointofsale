package test;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;

import org.junit.Test;

import database.Product;
import devices.lcd.ErrorCode;
import devices.lcd.LcdDisplay;

public class LcdDisplayTest { 
	
	@Test
	public void testDisplayProduct()
	{
		Product product = new Product("Name", "code", new BigDecimal(120));
		final OutputStream os = new ByteArrayOutputStream();
		LcdDisplay lcd = new LcdDisplay() {
			
			@Override
			protected OutputStream getOutputStream() {
				return os;
			}
		};
		try {
			lcd.displayProduct(product);
		} catch (IOException e) {
			e.printStackTrace();
		}
		byte[] bytes = ((ByteArrayOutputStream) os).toByteArray();
		String displayText = new String(bytes);
		assertEquals("Test display product", "Name\t120,00\n", displayText);
	}
	
	@Test
	public void testDisplaySumAfterExit()
	{
		BigDecimal sum = new BigDecimal(1200);
		final OutputStream os = new ByteArrayOutputStream();
		LcdDisplay lcd = new LcdDisplay() {
			
			@Override
			protected OutputStream getOutputStream() {
				return os;
			}
		};
		try {
			lcd.displaySum(sum);
		} catch (IOException e) {
			e.printStackTrace();
		}
		byte[] bytes = ((ByteArrayOutputStream) os).toByteArray();
		String displayText = new String(bytes);
		assertEquals("Test display sum", "Suma: \t1200,00\n", displayText);
	}
	
	@Test
	public void testDisplayInvalidBarCodeError()
	{
		final OutputStream os = new ByteArrayOutputStream();
		LcdDisplay lcd = new LcdDisplay() {
			
			@Override
			protected OutputStream getOutputStream() {
				return os;
			}
		};
		try {
			lcd.displayError(ErrorCode.INVALID_BAR_CODE);
		} catch (IOException e) {
			e.printStackTrace();
		}
		byte[] bytes = ((ByteArrayOutputStream) os).toByteArray();
		String displayText = new String(bytes);
		assertEquals("Test display sum", "Invalid bar-code\n", displayText);
	}
	
	@Test
	public void testDisplayProductNotFoundError()
	{
		final OutputStream os = new ByteArrayOutputStream();
		LcdDisplay lcd = new LcdDisplay() {
			
			@Override
			protected OutputStream getOutputStream() {
				return os;
			}
		};
		try {
			lcd.displayError(ErrorCode.PRODUCT_NOT_FOUND);
		} catch (IOException e) {
			e.printStackTrace();
		}
		byte[] bytes = ((ByteArrayOutputStream) os).toByteArray();
		String displayText = new String(bytes);
		assertEquals("Test display sum", "Product not found\n", displayText);
	}
}
