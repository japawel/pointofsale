package test;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.HashMap;

import org.junit.Test;

import database.DatabaseConnector;
import database.Product;

public class DatabaseTest {

	@Test
	public void test() 
	{	
		DatabaseConnector databaseConnector = getDatabase();
		Product product = databaseConnector.getProduct("Code");
		assertEquals("Test products", "Name", product.getName());
		assertEquals("Test products", "Code", product.getCode());
		assertEquals("Test products", new BigDecimal(120), product.getPrice());
		assertEquals("Test products", "120,00", product.getPriceStringFormat());
	}
	
	@Test
	public void testNotFoundProduct()
	{
		DatabaseConnector databaseConnector = getDatabase();
		Product product = databaseConnector.getProduct("Not added");
		assertNull(product);
	}
	
	private DatabaseConnector getDatabase()
	{
		DatabaseConnector databaseConnector = new DatabaseConnector() 
		{
			@Override
			public Product getProduct(String code) 
			{
				return getProducts().get(code);
			}
		};
		return databaseConnector;
	}
	
	private HashMap<String, Product> getProducts()
	{
		HashMap<String, Product> productsMap = new HashMap<String, Product>();
		productsMap.put("Code", new Product("Name", "Code", new BigDecimal(120)));
		productsMap.put("Code2", new Product("Name2", "Code2", new BigDecimal(110)));
		productsMap.put("Code3", new Product("Name3", "Code3", new BigDecimal(12.12)));
		productsMap.put("Code4", new Product("Name4", "Code4", new BigDecimal(1.5)));
		return productsMap;
	}
	

}
