import system.SaleSystem;
import database.DatabaseConnector;
import database.mock.MockDatabaseConnector;
import devices.lcd.LcdDisplay;
import devices.lcd.mock.MockLcdDisplay;
import devices.printer.Printer;
import devices.printer.mock.MockPrinter;
import devices.scanner.Scanner;
import devices.scanner.mock.MockScaner;


public class Main {

	private final static String DEFAULT_DATABASE_XML_PATH = "defaultXmlDatabase.xml";
	
	public static void main(String[] args) 
	{
		DatabaseConnector databaseConnector = new MockDatabaseConnector(DEFAULT_DATABASE_XML_PATH);
		Scanner scanner = new MockScaner();
		LcdDisplay lcdDisplay = new MockLcdDisplay();
		Printer printer = new MockPrinter();
		
		new SaleSystem(databaseConnector, scanner, lcdDisplay, printer);
	}
}
