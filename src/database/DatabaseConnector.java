package database;

public interface DatabaseConnector {

	public Product getProduct(String code);
	
}
