package database.mock;

import java.io.File;
import java.math.BigDecimal;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import database.DatabaseConnector;
import database.Product;

public class MockDatabaseConnector implements DatabaseConnector{

	private final static String PRODUCT_NODE_NAME = "product";
	private final static String	NAME_ATTR_NAME = "name";
	private final static String	CODE_ATTR_NAME = "code";
	private final static String PRICE_ATTR_NAME = "price";
	
	private HashMap<String, Product> avaiableProductsMap;
	
	public MockDatabaseConnector(String xmlDatabaseFile) 
	{
		readXmlDatabaseFile(xmlDatabaseFile);
	}
	
	@Override
	public Product getProduct(String code) 
	{
		return avaiableProductsMap.get(code);
	}
	
	private void readXmlDatabaseFile(String xmlDatabaseFile)
	{
		avaiableProductsMap = new HashMap<String, Product>();
		try 
		{
			File fXmlFile = new File(xmlDatabaseFile);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			
			NodeList productsList = doc.getElementsByTagName(PRODUCT_NODE_NAME);
			  
			for (int temp = 0; temp < productsList.getLength(); ++temp)
			{
				Node productNode = productsList.item(temp);
				if (productNode.getNodeType() == Node.ELEMENT_NODE) 
				{
					Element productElement = (Element) productNode;
					String strPrice = productElement.getAttribute(PRICE_ATTR_NAME);
					if (strPrice.isEmpty())
						continue;
					
					String name = productElement.getAttribute(NAME_ATTR_NAME);
					String code = productElement.getAttribute(CODE_ATTR_NAME);
					BigDecimal price = new BigDecimal(strPrice);
					avaiableProductsMap.put(code, new Product(name, code, price));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
