package database;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class Product {

	private String name;
	private String code;
	private BigDecimal price;
	
	public Product(String name, String code, BigDecimal price)
	{
		this.name = name;
		this.code = code;
		this.price = price;
	}
	
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public BigDecimal getPrice() 
	{
		return price;
	}
	
	public void setPrice(BigDecimal price) 
	{
		this.price = price;
	}

	public String getPriceStringFormat()
	{
		return new DecimalFormat("0.00").format(price);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
