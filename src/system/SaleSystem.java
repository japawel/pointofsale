package system;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import database.DatabaseConnector;
import database.Product;
import devices.lcd.ErrorCode;
import devices.lcd.LcdDisplay;
import devices.printer.Printer;
import devices.scanner.Scanner;

public class SaleSystem {
	
	private static final String EXIT_CODE = "exit";
	
	public SaleSystem(DatabaseConnector databaseConnector, Scanner scanner, LcdDisplay lcdDisplay, Printer printer)
	{
		init(databaseConnector, scanner, lcdDisplay, printer);
	}
	
	private void init(DatabaseConnector databaseConnector, Scanner scanner, LcdDisplay lcdDisplay, Printer printer)
	{
		String code = "";
		BigDecimal sum = new BigDecimal(0);
		
		List<Product> products = new ArrayList<Product>();
		while(!EXIT_CODE.equals(code))
		{
			try 
			{
				code = scanner.getCode();
				if ("".equals(code))
				{
					lcdDisplay.displayError(ErrorCode.INVALID_BAR_CODE);
					continue;
				}
				
				Product product = databaseConnector.getProduct(code);
				if (product == null)
				{
					lcdDisplay.displayError(ErrorCode.PRODUCT_NOT_FOUND);
					continue;
				}
				
				products.add(product);
				sum = sum.add(product.getPrice());
				lcdDisplay.displayProduct(product);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			lcdDisplay.displaySum(sum);
			printer.printRecipt(products);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
